public class Veterinarian {
    private String name;

    public Veterinarian(String name) {
        this.name = name;
    }
    public Veterinarian() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void treatment(Animal animal) {
        System.out.println("I treat animals, including cats and dogs");

    }

    @Override
    public String toString() {
        return "Veterinarian{" +
                "name='" + name + '\'' +
                '}';
    }
}
