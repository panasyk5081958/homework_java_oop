public class Dog extends Animal {
    private String name;

    public Dog(String name) {
        this.name = name;
    }
    public Dog() {

    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String getVoice() {
        return "gav gav";
    }

    @Override
    public void eat() {
        System.out.println("I like to eat a meat");
    }
    @Override
    public void sleep() {
        System.out.println("I wantn't to sleep at night sometimes");
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                "ration='" + getRation() + '\'' +
                ", color='" + getColor() + '\'' +
                ", weight=" + getWeight() +
                '}';
    }
}
