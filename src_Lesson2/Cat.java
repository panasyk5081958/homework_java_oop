public class Cat extends Animal{
        private String name;

        public Cat(String name) {
                this.name = name;
        }
        public Cat() {

        };

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }
        @Override
        public String getVoice() {
                return "meow meow";
        }

        @Override
        public void eat() {
                System.out.println("I eat a mouse");
        }
        @Override
        public void sleep() {
                System.out.println("I wantn't to sleep at night");
        }

        @Override
        public String toString() {
                return "Cat{" +
                        "name='" + name + '\'' +
                        "ration='" + getRation() + '\'' +
                        ", color='" + getColor() + '\'' +
                        ", weight=" + getWeight() +
                        '}';
        }
}
