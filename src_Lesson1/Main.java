public class Main {
    public static void main (String[] args) {
        Comp comp1 = new Comp();
        comp1.setType("Desktop");
        comp1.setPrice(800);
        comp1.setHeight(0.2);

        Comp comp2 = new Comp("Tower", 900, 0.5);
        Comp comp3 = comp2;
        comp3.setPrice(1000);

        System.out.println(comp1.toString());
        System.out.println(comp2.toString());
        System.out.println(comp1.cost());
        System.out.println(comp3.getPrice());

        Triangle triangle1 = new Triangle(8.0, 9.0, 11.0);
        Triangle triangle2 = new Triangle(3.0, 4.0, 5.0);

        System.out.println(triangle1.toString());
        System.out.println(triangle2.toString());


    }
}
