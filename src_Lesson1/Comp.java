public class Comp {
    private String type;
    private double price;
    private double height;

    public Comp(String type, double price, double height) {
        this.type = type;
        this.price = price;
        this.height = height;
    }
    public Comp() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    public double cost() {
        double buy = (price * 5);
        return  (buy);
    }

    @Override
    public String toString() {
        return "Comp{" +
                "type='" + type + '\'' +
                ", price=" + price +
                ", height=" + height +
                '}';
    }
}

